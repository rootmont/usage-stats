FROM python:3.7

COPY ./common /common
COPY ./usage /usage
COPY ./requirements.txt /usage
WORKDIR /usage
RUN pip install -r requirements.txt

CMD ["python", "main.py"]