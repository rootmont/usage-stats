from web3 import HTTPProvider, Web3
from common.env import INFURAPROJECTID

transfer_topic = '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef'

infura_url = 'https://mainnet.infura.io/v3/%s' % INFURAPROJECTID
provider = HTTPProvider(infura_url)
web3 = Web3(provider)

backfill_all_ntxs = False
seconds_per_day = 60 * 60 * 24
seconds_per_eth_block = 17
blocks_per_day = seconds_per_day // seconds_per_eth_block
