import datetime
import pandas as pd

from common.db import blocks
from usage.sensors import eos, ethereum
from common.config import maybe_error, maybe_retry, maybe_parallel

import logging
logger = logging.getLogger('gathering')


def get_chain_height(blockchain):
    if blockchain == 'ethereum':
        return ethereum.get_chain_height()
    elif blockchain == 'eos':
        return eos.get_chain_height()
    # elif blockchain == 'cardano':
    #     return cardano.get_chain_height()
    else:
        logger.error('Unkown blockchain: %s' % blockchain)
        return None


def get_block_net(blockchain, blocknum):
    if blockchain == 'ethereum':
        block = ethereum.get_block(blocknum)
    elif blockchain == 'eos':
        block = eos.get_block(blocknum)
    # elif blockchain == 'cardano':
    #     return cardano.get_block(blocknum)
    else:
        logger.error('Unknown blockchain: %s' % blockchain)
        return None
    if block is None:
        raise ValueError('Unable to get block {} from {} network'.format(blocknum, blockchain))
    return block

def get_blocks_by_date(days_back, blockchain, parallel=True):
    sync_db(blockchain, parallel)
    start_date = datetime.date.today() - datetime.timedelta(days_back + 1)
    df = blocks.get_blocks_from_date(start_date, blockchain)
    gb = df.groupby(['my_date'])['blocknumber'].min().to_dict()
    return gb


def get_block_net_retry(args):
    block = maybe_error(maybe_retry, (get_block_net, args))
    if block is None:
        logger.warning('Failed to get block: {}'.format(args))
        block = {}
    else:
        blockchain, _ = args
        logger.debug('Inserting %s block %s' % (blockchain, block))
        blocks.insert_block(blocknumber=block['blocknumber'], blockchain=blockchain, datetime=block['datetime'], txs=block['txs'])
    return block


def insert_all_blocks(blockchain, parallel=True):
    chain_height = get_chain_height(blockchain)
    _ = get_blocks(1, chain_height, blockchain, parallel)


def sync_db(blockchain, parallel=True):
    latest_db = blocks.get_latest_block(blockchain)
    if latest_db is None:
        latest_db = 0
    chain_height = get_chain_height(blockchain)
    _ = get_blocks(start=latest_db + 1, end=chain_height, blockchain=blockchain, parallel=parallel)


def get_blocks(start, end, blockchain, parallel=True):
    db_blocks = blocks.get_blocks(start, end, blockchain)
    missing = set(range(start, end+1)) - {x['blocknumber'] for x in db_blocks}
    args = [[blockchain, x] for x in missing]
    logger.info('Retrieved %d %s blocks from db, getting %d from network' % (len(db_blocks), blockchain, len(missing)))
    # delete this before forking, bc memory usage skyrockets
    del(missing)
    net_blocks = maybe_parallel(get_block_net_retry, args, parallel, num_processes=20)

    return pd.DataFrame(db_blocks + net_blocks)
