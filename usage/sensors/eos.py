from dateutil import parser as dateparser
from common.db import blocks, timeseries
from common.config import maybe_request, maybe, format_logger, earliest_eos_date
from usage.sensors import xfers
import requests
import json
import logging
import datetime
from time import sleep

logger = logging.getLogger('eos-block-stream')
format_logger(logger)


def get_chain_height():
    url = 'https://eos.greymass.com/v1/chain/get_info'
    j = requests.get(url).json()
    txt = maybe_request([url, {}], stall=0)
    if txt is None:
        logger.error('Could not get num eos blocks')
        return None
    j = json.loads(txt)
    num_blocks = j['last_irreversible_block_num']
    return num_blocks


def get_block(blocknum):
    payload = json.dumps({'block_num_or_id': str(blocknum)})
    res = requests.post('https://eos.greymass.com/v1/chain/get_block', data=payload)
    if res.status_code != 200:
        return None
    data = json.loads(res.text)
    return {
        'txs': len(data['transactions']),
        'blocknumber': blocknum,
        'datetime': dateparser.parse(data['timestamp'])
    }


def stream_blocks(start_block=0, sleep_time=5):
    from usage.ntx import backfill_ntx_retry
    if start_block == 0:
        logger.debug('start_block=0, retrieving last block collected')
        last_block_collected = blocks.get_latest_block('eos')
        if last_block_collected is None: last_block_collected = 1
    else:
        last_block_collected = start_block
    logger.debug('last_block_collected={}'.format(last_block_collected))

    while True:
        tip = get_chain_height()
        logger.debug('chain_height = {}'.format(tip))
        if tip is None: continue
        if tip >= last_block_collected:
            block = get_block(last_block_collected)
            if block is None: continue
            logger.info('Inserting eos block %s' % (block))
            blocks.insert_block(blocknumber=block['blocknumber'], blockchain='eos', datetime=block['datetime'],
                                txs=block['txs'])
            logger.info('Checkpoint - Inserting eos block %s' % (block))
            last_block_collected += 1
        else:
            latest = timeseries.get_latest_ntxs()
            latest_date = latest.get('EOS', earliest_eos_date + datetime.timedelta(days=1))
            ntxs = {'EOS': xfers.get_ntx_from_db(latest_date - datetime.timedelta(days=1), 'eos')}
            logger.info('Backfilling ntxs from %s' % latest_date)
            maybe(backfill_ntx_retry, ntxs.items(), num_processes=1)
            logger.info('Sleeping for %d seconds' % sleep_time)
            sleep(sleep_time)
