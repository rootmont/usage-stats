import requests
import re

from common.config import maybe_request


def get_all_coins():
    url = 'https://bitinfocharts.com/'
    txt = requests.get(url).text
    return re.findall(pattern="title='([a-zA-Z]*?)'", string=txt)


def get_bitinfo_ntx_coins():
    url = 'https://bitinfocharts.com/comparison/bitcoin-transactions.html'
    text = maybe_request([url, {}], num_retries=3)
    if text is None: return []
    f = re.findall(pattern='data-coin="(.*?)"', string=text)
    return [x.upper() for x in f]

