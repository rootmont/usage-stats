from common.db import blocks, timeseries
from time import sleep
import datetime
from usage.config import seconds_per_eth_block, web3
from common.config import maybe, format_logger, utc
from usage.sensors import xfers
import logging

logger = logging.getLogger('ethereum-block-stream')
format_logger(logger)


def get_chain_height():
    return web3.eth.blockNumber


def get_block(blocknum):
    block = web3.eth.getBlock(blocknum)
    if block is None: return None
    return {
        'txs': len(block['transactions']),
        'blocknumber': blocknum,
        'datetime': datetime.datetime.fromtimestamp(block['timestamp'], utc)
    }


def stream_blocks(start_block=0):
    from usage.ntx import backfill_ntx_retry

    if start_block == 0:
        last_block_collected = blocks.get_latest_block('ethereum')
        if last_block_collected is None: last_block_collected = 1
    else:
        last_block_collected = start_block

    logger.info('Streaming ethereum blocks')
    while True:
        try:
            tip = get_chain_height()
            if tip >= last_block_collected:
                block = get_block(last_block_collected)
                if block is None: continue
                logger.debug('Inserting ethereum block %s' % (block))
                blocks.insert_block(blocknumber=block['blocknumber'], blockchain='ethereum', datetime=block['datetime'],
                                    txs=block['txs'])
                last_block_collected += 1
            else:
                latest = timeseries.get_latest_ntxs()
                latest_eth = latest['Ethereum']
                ntxs = {'Ethereum': xfers.get_ntx_from_db(latest_eth - datetime.timedelta(days=1), 'ethereum')}
                logger.debug('Backfilling ntxs from %s' % latest_eth)
                maybe(backfill_ntx_retry, ntxs.items(), num_processes=1)
                logger.debug('Sleeping for %d seconds' % seconds_per_eth_block)
                sleep(seconds_per_eth_block)
        except Exception as e:
            logger.error(e)
            sleep(seconds_per_eth_block)
