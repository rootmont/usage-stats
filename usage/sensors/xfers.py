import datetime
import json
import re
import time
import requests
from common.db import blocks
from common.db.connect import timing
from common.config import maybe_retry, utc, firefox_headers
from usage.config import transfer_topic, web3, infura_url
import logging
import pandas as pd
from time import sleep
logger = logging.getLogger('gathering')



def get_ntx_from_db(start_date, blockchain):
    blox = blocks.get_blocks_from_date(start_date, blockchain)
    return blox.groupby(['my_date'])['txs'].sum().to_dict()


def get_ntx_bitinfo_retry(args):
    return maybe_retry(get_ntx_bitinfo, [args])


def get_ntx_bitinfo(arglist):
    logger.debug('Getting %d ntx from bitinfo' % len(arglist))
    i = 0
    syms = [x[1] for x in arglist]
    failure_count = 0
    df = None
    while i < len(syms) and failure_count < 5:
        sym_seg = syms[i:i+10]
        sym_str = '-'.join(sym_seg)
        url = "https://bitinfocharts.com/comparison/transactions-%s.html" % sym_str
        res = requests.get(url, allow_redirects=False).text
        start = 'new Dygraph(document.getElementById("container"),'
        end = ', {labels: ["Date"'
        remove = "new Date("
        try:
            datastring = res.split(start)[1].split(end)[0].replace(remove, "").replace(")", "")
            j = json.loads(datastring)
            if i == 0:
                df = pd.DataFrame(columns=['datestring'] + syms, index=[x[0] for x in j])
            cols = ['datestring'] + sym_seg
            df2 = pd.DataFrame(j, columns=cols)
            df2.index = df2['datestring']
            df.loc[df2.index, cols] = df2
            i += 10
        except Exception:
            failure_count += 1
            logger.warning('No bitinfocharts ntx data for %s, sleeping for 10 seconds' % url)
            sleep(10)

    # df['date'] = df['datestring'].apply(lambda x: datetime.datetime.strptime(x, '%Y/%m/%d').date())
    # df.index = df['date']
    if df is None:
        return None
    df.index = df['datestring']
    ret = {}
    for arg in arglist:
        name, sym, missing_dates = arg
        missing_datestrings = [x.strftime('%Y/%m/%d') for x in missing_dates]
        # filter out nan
        non_null = df[df[sym].isnull() == False]
        # intersect with missing dates
        new_index = non_null.index.intersection(missing_datestrings)
        # store in return dict
        ret[name] = df.loc[new_index][sym].to_dict()

    return ret


def get_ntx_cryptoid(args):
    name, symbol, missing_dates, chain_height = args
    #start_dt = datetime.datetime(start_date.year, start_date.month, start_date.day, 17, 0, 0)
    logger.debug('Getting ntx data from cryptoid for %s' % (name))
    #blocknum = chain_height
    # { date: ntx }
    ntx = {}
    for date in missing_dates:
        day_num = get_day_num_cryptoid(date)
        n = get_ntx_for_day_cryptoid(symbol, day_num)
        if n is None: break
        if type(n) is not int:
            logger.error('Bad ntx for %s %d' % (symbol, day_num))
        if n < 0:
            logger.warning('Negative ntx for %s %d: %d' % (symbol, day_num, n))
            break
        ntx[date] = n
    return {'name': name, 'ntx': ntx}


def get_ntx_for_day_cryptoid(symbol, day_num):
    logger.debug('Getting ntx for %s on %d' % (symbol, day_num))
    url = 'https://chainz.cryptoid.info/explorer/overview.day.dws?coin=%s&d=%d&fmt.js' % (symbol.lower(), day_num)
    try:
        text = requests.get(url, headers=firefox_headers, timeout=120).text
        j = json.loads(text)
        ntx = j.get('txc')
    except json.JSONDecodeError as e:
        logger.error("Could not decode json %s" % text)
        ntx = 0
    except requests.exceptions.ProxyError as e:
        logger.error("Could not complete request to %s" % url)
        ntx = 0
    return ntx


def get_date_cryptoid(day_num):
    ts = (day_num - 25569) * 86400
    return datetime.datetime.fromtimestamp(ts, utc)


def get_day_num_cryptoid(d):
    dt = datetime.datetime(d.year, d.month, d.day)
    day_num = (dt.timestamp() / 86400) + 25569
    return round(day_num)


def get_block_cryptoid(symbol, blocknum):
    url = "https://chainz.cryptoid.info/%s/block.dws?%d.htm" % (symbol.lower(), blocknum)
    try:
        text = requests.get(url, headers=firefox_headers, timeout=60).text
        if 'Too Many Requests' in text:
            logger.warning('Too many requests to cryptoid, waiting 5 minutes')
            time.sleep(60 * 5)
        ntx = re.findall('Transactions.*?([0-9]+)\r', text, re.DOTALL)[0]
        ntx = int(ntx)
        date = re.findall('Date/Time.*?([0-9]{4}.+?) ', text, re.DOTALL)[0]
        return ntx, date
    except Exception as e:
        logger.error('Could not get cryptoid ntx from %s %s' % (url, e))
        return 0, None


def get_cryptoid_coins():
    url = "https://chainz.cryptoid.info/stats.dws"
    text = requests.get(url, headers=firefox_headers, timeout=60).text
    return json.loads(text)

@timing
def get_transfers_for_token(token_addr, fromBlock, toBlock):
    if not web3.isAddress(token_addr):
        logger.warning('%s is not a web3 address' % token_addr)
        return []
    if fromBlock is None:
        logger.warning('fromBlock is None')
        return []

    if toBlock == 'latest':
        toBlock = web3.eth.blockNumber
    results = []
    delta = 10000
    fromBlockTemp = toBlock - delta
    sleep_time = 1
    headers = {'Content-Type': 'application/json'}
    while True:
        logger.debug('Getting ntx for token %s from block %d to block %d' % (token_addr, fromBlockTemp, toBlock))
        data = {
            'jsonrpc': '2.0',
            'id': 1,
            'method': 'eth_getLogs',
            'params': [
                {
                    'topics': [
                        transfer_topic
                    ],
                    'address': token_addr,
                    'fromBlock': hex(fromBlockTemp),
                    'toBlock': hex(toBlock)
                }
            ]
        }
        try:
            response = requests.post(url=infura_url, timeout=300, json=data, headers=headers)
        except requests.ConnectionError:
            logger.error('Connection Error for' + infura_url)
            continue

        if response.status_code == 200 and '502 Bad Gateway' not in response.text:
            data = json.loads(response.text)
            if 'result' in data:
                results.extend(data['result'])
                if fromBlockTemp == fromBlock: return results
                toBlock = fromBlockTemp - 1
                fromBlockTemp = max(fromBlock, fromBlockTemp - delta)
            elif 'error' in data:
                logger.warning('Infura returned message without result: {}, halving block window'.format(data['error']))
                fromBlockTemp = (fromBlockTemp + toBlock) // 2
            else:
                logger.error('Bad data from %s:%s, retrying after sleeping for %d' % (infura_url, data, sleep_time))
                sleep(sleep_time)
                # exponential backoff
                sleep_time <<= 1
        # TODO: this path may be obsolete with the new api version
        else:
            logger.error('Bad response for token %s: %d %s: halving block window in case too much data' % (token_addr, response.status_code, response.text))
            fromBlockTemp = (fromBlockTemp + toBlock) // 2

