import json
import datetime
import logging

from common.config import maybe_parallel, format_logger, utc, maybe_request

logger = logging.getLogger('sensors.cardano')
format_logger(logger)
blocknum_multiplier = 100000


def get_date_for_epoch_cardano(epoch):
    stem = 'https://cardanoexplorer.com/api/epochs/%d?page=' % epoch
    url = '%s%d' % (stem, 1)
    txt = maybe_request([url, {}])
    if txt is None: return None
    j = json.loads(txt)
    if 'Left' in j: return None
    # go to last page
    num_pages = j['Right'][0]
    url = '%s%d' % (stem, num_pages)
    txt = maybe_request([url, {}], stall=0)
    if txt is None: return None
    j = json.loads(txt)
    if 'Left' in j: return None
    last_date = datetime.datetime.fromtimestamp(j['Right'][1][0]['cbeTimeIssued'], utc).date()
    return last_date, num_pages


def get_ntx_cardano(start_date, parallel=True):
    epoch = 0
    # ntx = {date: ntxs}
    ntxs = {}
    while True:
        date, num_pages = get_date_for_epoch_cardano(epoch)
        if date is None: break
        if date < start_date: continue
        args = [(epoch,page) for page in range(1, num_pages+1)]
        results = maybe_parallel(fxn=get_ntx_cardano_epoch_page,args=args,parallel=parallel)
        for result in results:
            for date in result:
                try:
                    ntxs[date] += result[date]
                except KeyError:
                    ntxs[date] = result[date]
        epoch += 1
    return {'Cardano': ntxs}


def get_ntx_cardano_epoch_page(args):
    epoch, page = args
    stem = 'https://cardanoexplorer.com/api/epochs/%d?page=' % epoch
    ntxs = {}
    url = '%s%d' % (stem, page)
    txt = maybe_request([url, {}], stall=0)
    if txt is None: return ntxs
    j = json.loads(txt)
    if 'Left' in j: return ntxs
    for slot in j['Right'][1]:
        date = datetime.datetime.fromtimestamp(slot['cbeTimeIssued'], utc).date()
        try:
            ntxs[date] += slot['cbeTxNum']
        except KeyError:
            ntxs[date] = slot['cbeTxNum']
    return ntxs


def epoch_slot_to_blocknum(epoch, slot):
    if slot >= blocknum_multiplier:
        raise Exception('Page %s too large, violates blocknumber scheme' % slot)
    return (epoch * blocknum_multiplier) + slot


def blocknum_to_epoch_slot(blocknum):
    epoch = blocknum // blocknum_multiplier
    slot = blocknum % blocknum_multiplier
    return epoch, slot


def get_chain_height():
    url = 'https://cardanoexplorer.com/socket.io/?EIO=3&transport=polling&t=MVsOkHC&sid=GzcZNhABFDg%2FCjsFMygs'
    txt = maybe_request([url, {}], stall=0)
    if txt is None:
        logger.error('Could not get num cardano blocks')
        return None
    # cut off first 8 bytes, since this is actually a websocket connection
    j = json.loads(txt[8:])
    num_epochs = j[1][1][1]['cbeEpoch']
    num_slots = j[1][1][1]['cbeSlot']
    num_blocks = epoch_slot_to_blocknum(num_epochs, num_slots)
    return num_blocks


# def get_block(blocknum):
#     payload = json.dumps({'block_num_or_id': str(blocknum)})
#     res = requests.post('https://eos.greymass.com/v1/chain/get_block', data=payload)
#     if res.status_code != 200:
#         raise Exception
#     data = json.loads(res.text)
#     return {
#         'txs': len(data['transactions']),
#         'blocknumber': blocknum,
#         'datetime': dateparser.parse(data['timestamp'])
#     }
#
