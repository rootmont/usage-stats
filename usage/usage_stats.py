import pandas as pd
from scipy.stats import linregress
import logging

from common.db import coin, timeseries
from common.rank_tree import rank_tree
from common.config import format_logger, age_clusters, mcap_clusters, industry_clusters, sliding_window_sizes
from common.config import maybe_parallel
from common.stats.statsbase import StatsBase


class UsageStats(StatsBase):
    stat_type = 'usage'
    logger = logging.getLogger('usage-stats')
    format_logger(logger)

    @classmethod
    def format_stats(cls, date, stats):
        metric_args = []
        benchmark_args = []
        rank_args = []
        # stats =  { time: { cluster: [statz, rank, pctile, summary] } }
        for time, d in stats.items():
            for cluster, dd in d.items():
                cluster_type = cls.cluster_to_cluster_type(cluster)
                statz, rank, percentile, summary = dd
                # first do metrics
                for metric in statz:
                    smr = summary[metric]
                    derived_metric = '%s_%s' % (metric, time)
                    benchmark_args.append(
                        [date, cluster_type, cluster, derived_metric, smr['mean'], smr['std'], smr['min'], smr['25%'],
                         smr['50%'], smr['75%'], smr['max'], smr['count']])
                    for coin in statz.index:
                        value = statz[metric][coin]
                        metric_args.append([time, None, metric, coin, value])
                # then ranks
                for metric in rank:
                    for coin in rank.index:
                        value = rank[metric][coin]
                        rank_args.append([time, cluster_type, metric, coin, value])
                # then percentile
                for coin in percentile.index:
                    rank_args.append([time, 'global', cls.stat_type, coin, percentile.loc[coin]])
        organized = {x[3]: {'date': date} for x in metric_args}
        for x in metric_args:
            col = cls.get_column_name(x, rank=False)
            name, value = x[3:]
            organized[name][col] = value
        for x in rank_args:
            col = cls.get_column_name(x, rank=True)
            name, value = x[3:]
            organized[name][col] = value

        return organized, benchmark_args

    @classmethod
    def get_stats_for_date(cls, end_date):
        usage = {}
        for time in sliding_window_sizes:
            u = cls._get_usage(end_date=end_date, days_back=time)
            if u is None:
                return {}
            usage[str(time)] = u

        ret = {}

        # filter and pack it
        for time, df in usage.items():
            ret[time] = {}
            for age_cluster in age_clusters:
                cls.logger.debug('computing usage stats for age cluster: {}'.format(age_cluster))
                coins = coin.get_coin_info_age(age_cluster)
                if coins is None: continue
                ret[time][age_cluster] = cls._filter(df, coins)
            for mcap_cluster in mcap_clusters:
                cls.logger.debug('computing usage stats for mcap cluster: {}'.format(mcap_cluster))
                coins = coin.get_coin_info_marketcap(mcap_cluster)
                if coins is None: continue
                ret[time][mcap_cluster] = cls._filter(df, coins)
            for industry_cluster in industry_clusters:
                cls.logger.debug('computing usage stats for industry cluster: {}'.format(industry_cluster))
                coins = coin.get_coin_info_industry(industry_cluster)
                if coins is None: continue
                ret[time][industry_cluster] = cls._filter(df, coins)
            cls.logger.debug('computing usage stats globally: {}'.format(age_cluster))
            coins = coin.get_coin_info_all()
            ret[time]['global'] = cls._filter(df, coins)

        return ret

    @classmethod
    def insert(cls, formatted):
        print("Inserting to timeseries... ")
        maybe_parallel(timeseries.insert, formatted.items(), parallel=False)

    @classmethod
    def _get_usage(cls, end_date, days_back):
        cls.logger.debug('Getting usage stats for {}, starting {} days back'.format(end_date, days_back))
        
        raw = timeseries.get_recent_transactions(end_date=end_date, days_back=days_back).fillna(0)
        
        cls.logger.debug('Getting raw transactions stats for {}, starting {} days back'.format(end_date, days_back))
        
        cls.logger.debug('Raw get_recent_transactions: {}'.format(raw))
        
        if len(raw) == 0:
            cls.logger.warning('No ntx data found for {}'.format(end_date))
            return
            
        df = raw.pivot("my_date", "token_name").fillna(0)
        df.rename(columns={'price_close': 'price', 'daily_transactions': 'ntx', 'my_date': 'date'}, inplace=True)
        df.columns = df.columns.set_names(['metric', 'token_name'])
        ntxs = df["ntx"]
        prices = df["price"]
        log_prices = pd.np.log(1 + prices)
        log_ntxs = pd.np.log(1 + ntxs)

        meta_cols = [
            'metcalfe_exponent',
            'metcalfe_proportion',
            'metcalfe_correlation'
        ]

        tseries_cols = [
            'price',
            'metcalfe_movement',
            'metcalfe_price_ratio',
            'normalized_metcalfe',
            'daily_transactions',
            'ntx_divergence',
            'raw_metcalfe',
            'trading_volume',
            'marketcap'
        ]

        time_index = prices.index
        metric_cols = meta_cols + tseries_cols
        names = prices.columns

        meta = pd.DataFrame(columns=meta_cols, index=names)
        tseries = pd.DataFrame(columns=pd.MultiIndex.from_product([tseries_cols, names]), index=time_index)
        tseries['daily_transactions'] = ntxs
        tseries['price'] = prices
        tseries['marketcap'] = df['marketcap']
        tseries['trading_volume'] = df["trading_volume"]

        cls.logger.debug('Starting Computing usage stats for {} elemmnts'.format(len(prices.columns)))
        for token_name in prices.columns:
            rel = (log_ntxs[token_name] > 0) & (log_prices[token_name] > 0)
            rel_log_ntx = log_ntxs[token_name][rel]
            rel_log_price = log_prices[token_name][rel]
            if len(rel_log_ntx) == 0 or len(rel_log_price) == 0 or all(ntxs[token_name] == 0):
                meta['metcalfe_exponent'][token_name] = 0
                meta['metcalfe_proportion'][token_name] = 0
                meta['metcalfe_correlation'][token_name] = 0
                tseries.loc[:, ('raw_metcalfe',token_name)] = 0
                tseries.loc[:, ('normalized_metcalfe',token_name)] = 0
                tseries.loc[:, ('ntx_divergence',token_name)] = 0
                tseries.loc[:, ('metcalfe_price_ratio',token_name)] = 0
            else:
                # log(y) = m * log(x) + b ==> y = x**m * e**b
                # price = (ntx ** metcalfe_exponent) * metcalfe_proportion
                metcalfe_exponent, metcalfe_proportion, _, _, _ = linregress(rel_log_ntx, rel_log_price)
                meta['metcalfe_exponent'][token_name] = metcalfe_exponent
                meta['metcalfe_proportion'][token_name] = pd.np.e ** metcalfe_proportion
                # assign to all rows (dates) for the multi-column (metric, token_name)
                tseries.loc[:, ('raw_metcalfe', token_name)] = ntxs[token_name] ** metcalfe_exponent
                # must do this before taking the correlation
                tseries.loc[:, ('raw_metcalfe', token_name)].replace(pd.np.inf, 0, inplace=True)
                tseries.loc[:, ('normalized_metcalfe', token_name)] = tseries.loc[:, ('raw_metcalfe',token_name)] * meta['metcalfe_proportion'][token_name]
                tseries.loc[:, ('metcalfe_movement',token_name)] = tseries.loc[:, ('normalized_metcalfe',token_name)].pct_change()
                tseries.loc[:, ('ntx_divergence',token_name)] = tseries.loc[:, ('normalized_metcalfe',token_name)] - prices[token_name]
                tseries.loc[:, ('metcalfe_price_ratio',token_name)] = tseries.loc[:, ('normalized_metcalfe',token_name)] / prices[token_name]

                rel = (prices[token_name] > 0) & (tseries['normalized_metcalfe'][token_name].notnull())
                rel_prices = prices[token_name][rel]
                rel_met = tseries.loc[rel, ('normalized_metcalfe',token_name)]
                meta['metcalfe_correlation'][token_name] = rel_prices.corr(rel_met)

        cls.logger.debug('Finishing Computing usage stats for {} elemmnts'.format(len(prices.columns)))

        # clean, don't fillna on tseries bc mean will skip it and we need that for accuracy
        tseries = tseries.replace(pd.np.inf, 0)
        meta = meta.fillna(0).replace(pd.np.inf, 0)

        # take mean first to collapse time axis (index) before combining with meta
        ret = tseries.mean(axis=0, skipna=True)

        # reindex to include new metrics
        levels = [metric_cols, names]
        new_index = pd.MultiIndex.from_product(levels)
        ret = ret.reindex(new_index)

        # add meta to ret
        for col in meta_cols:
            ret[col] = meta[col]

        ret.fillna(0, inplace=True)

        # unstack to make the series with (a,b) multi-index become a dataframe with index b and columns a
        final = ret.unstack(level=0)

        cls.logger.debug('Getting final response in method _get_usage')

        cls.logger.debug(final.head())
        
        cls.logger.debug('Columns available in output: {}'.format(final.columns))

        return final

    @classmethod
    def _filter(cls, stats, coins):
        names = [x['token_name'] for x in coins if x['token_name'] in stats['price'].index]
        filtered = stats.loc[names]
        current_columns = set(filtered.columns)
        desired_columns = set(rank_tree['root_rank']['usage_rank'].keys())
        missing_columns = desired_columns - current_columns
        unnecessary_columns = current_columns - desired_columns
        if len(missing_columns) > 0:
            cls.logger.warning('Missing desired columns for usage stats: {}'.format(missing_columns))
        if len(unnecessary_columns) > 0:
            cls.logger.debug('Dropping unnecessary columns for usage distribution stats: {}'.format(unnecessary_columns))

        # take rank down index, ranking coins against each other
        rank = stats.rank(axis='index', pct=True)

        # drop the unnecessary columns before computing next level rank
        dropped = rank.drop(inplace=False, axis='columns', columns=unnecessary_columns)

        # overall pctile is mean across all metrics
        pctile = dropped.mean(axis='columns')

        # describe returns quantiles per column (metric in this case)
        summary = stats.describe().fillna(0)

        return filtered, rank, pctile, summary
