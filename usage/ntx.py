from common.db import timeseries, blocks
from common.config import maybe_error, maybe, maybe_retry, maybe_parallel, date_interval, format_logger, earliest_crypto_date
from usage.sensors import xfers, bitinfo, blockchains
import pandas as pd
import datetime
import logging
from common.db.connect import timing

logger = logging.getLogger('ntx')
format_logger(logger)


def backfill_ntx(args):
    name, ntxs = args
    for date in ntxs:
        ntx = ntxs[date]
        if ntx is None:
            continue
        logger.debug('Inserting ntx: %s, %s, %s' % (name, date, ntx))
        timeseries.insert_ntx(name=name, datestring=date, ntxs=ntx)


def backfill_ntx_retry(args):
    maybe_retry(backfill_ntx, args)


def update_ntxs(coin_data, parallel=True, backfill_all=False):
    logger.info('Updating ntxs')
    already_acquired = {
        'Ethereum': True,
        'EOS': True
    }

    if backfill_all:
        earliest = timeseries.get_earliest_prices()
        missing_dates = {
            coin['name'] : date_interval(earliest.get(coin['name'], earliest_crypto_date), datetime.date.today(), 1)
            for coin in coin_data
        }
    else:
        missing_ntxs = maybe_retry(timeseries.get_missing_ntxs,[], num_retries=5)
        missing_dates = {
            coin['name'] : missing_ntxs[missing_ntxs['token_name'] == coin['name']]['my_date']
            for coin in coin_data
            if len(missing_ntxs[missing_ntxs['token_name'] == coin['name']]['my_date']) > 0
        }

    # ntxs = { name: { date: ntx } }

    # get ethereum (handled now by stream_blocks)
    # update_ntx_eth(min(missing_dates.get('Ethereum',[datetime.date.today()])), parallel)
    # already_acquired['Ethereum'] = True

    # get eos (also handled by stream_blocks)
    # update_ntx_eos(min(missing_dates.get('EOS',[datetime.date.today()])), parallel)
    # already_acquired['EOS'] = True

    # get some ntx from infura
    new_ntxs = update_ntx_erc20(coin_data, missing_dates, already_acquired, parallel)
    if new_ntxs is not None:
        already_acquired.update({x.lower(): True for x in new_ntxs.keys()})

    # get some from bitinfo
    new_ntxs = update_ntx_bitinfo(coin_data, missing_dates, already_acquired)
    if new_ntxs is not None:
        already_acquired.update({x.lower(): True for x in new_ntxs.keys()})

    # get some from cryptoid
    new_ntxs = update_ntx_cryptoid(coin_data, missing_dates, already_acquired, parallel=parallel)
    if new_ntxs is not None:
        already_acquired.update({x.lower(): True for x in new_ntxs.keys()})

    # get cardano
    # new_ntxs = cardano.get_ntx_cardano(min(missing_dates.get('Cardano',[datetime.date.today()])), parallel)
    # maybe(backfill_ntxs, new_ntxs.items(), num_processes=1)
    # already_acquired['Cardano'] = True


# def update_ntx_cardano(start_date, parallel):
#     #sensors.blocks.sync_db('cardano', parallel)
#     ntxs = {'Cardano': ntx.get_ntx_from_db(start_date - datetime.timedelta(1), 'cardano') }
#     maybe(backfill_ntxs, ntxs.items(), num_processes=1)


def update_ntx_eth(start_date, parallel):
    blockchains.sync_db('ethereum', parallel)
    ntxs = {'Ethereum': xfers.get_ntx_from_db(start_date - datetime.timedelta(1), 'ethereum')}
    maybe(backfill_ntx_retry, ntxs.items(), num_processes=1)


def update_ntx_eos(start_date, parallel):
    blockchains.sync_db('eos', parallel)
    ntxs = {'EOS': xfers.get_ntx_from_db(start_date - datetime.timedelta(1), 'eos')}
    maybe(backfill_ntx_retry, ntxs.items(), num_processes=1)


def update_ntx_erc20(coin_data, missing_dates, already_acquired, parallel=True):

    blockchains.sync_db(blockchain='ethereum', parallel=parallel)
    args = [
        [
            coin['name'],
            coin['contract_address'].replace('\r',''),
            min(missing_dates.get(coin['name'], [datetime.date.today()]))
        ]
        for coin in coin_data
        if coin.get('hash_algorithm', '') == 'ERC20'
        and coin['name'].lower() not in already_acquired
    ]
    if len(args) == 0:
        logger.warning('No erc20 coins found')
        return

    logger.info('Collecting ntx data for %d erc20s' % len(args))
    results = maybe_parallel(fxn=get_ntx_erc20_retry, args=args, parallel=parallel, num_processes=25)

    if results is None:
        logger.warning('No ntxs collected for erc20s')
        return {}

    ntx = {
        x[0]: x[1]
        for x in results
        if x is not None
    }
    logger.info('Finished collecting ntx for erc20')

    return ntx


def update_ntx_bitinfo(coin_data, missing_dates, already_acquired):
    eligible_syms = bitinfo.get_bitinfo_ntx_coins()
    args = [
        [coin['name'], coin['symbol'].lower(), list(missing_dates.get(coin['name'], []))]
        for coin in coin_data
        if coin['name'].lower() not in already_acquired
        and coin['symbol'].upper() in eligible_syms
    ]
    logger.info('Collecting ntx from bitinfo')
    results = xfers.get_ntx_bitinfo_retry(args)
    if results is None:
        logger.warning('No ntxs found at bitinfocharts')
        return None
    logger.debug('Backfilling ntx from bitinfo')
    maybe(backfill_ntx_retry, results.items(), num_processes=1)

    return results


def update_ntx_cryptoid(coin_data, missing_dates, already_acquired, parallel):
    new_ntxs = get_ntx_cryptoid(coin_data, missing_dates, already_acquired, parallel)
    maybe(backfill_ntx_retry, new_ntxs.items(), num_processes=1)
    return new_ntxs


def get_ntx_cryptoid(coin_data, missing_dates, already_acquired, parallel=True):
    logger.info('Collecting ntx from cryptoid')
    cryptoids = xfers.get_cryptoid_coins()
    args = []
    for cryptoid in cryptoids:
        if len(cryptoid) == 0:
            continue
        cryptoid_name = cryptoid[1].lower()
        if cryptoid_name in already_acquired: continue
        cryptoid_chain_height = cryptoid[2]
        coins = [x for x in coin_data if x['name'].lower() == cryptoid_name]
        if len(coins) == 0:
            continue
        coin = coins[0]
        args.append([
            coin['name'],
            coin['symbol'],
            missing_dates.get(coin['name'], [datetime.date.today()]),
            cryptoid_chain_height
        ])
    results = maybe_parallel(xfers.get_ntx_cryptoid, args, parallel, num_processes=30)
    logger.info('Finished collecting ntx from cryptoid')
    return {ntxs['name']: ntxs['ntx'] for ntxs in results if len(ntxs['ntx']) > 0}


block_date_map = None
def get_date_for_blocknum(blocknumber):
    # just hold it in memory until container reload, the correct answers don't change
    global block_date_map
    if block_date_map is None or len(block_date_map) == 0:
        block_date_map = {}
        max_block = None
    else:
        max_block = min(block_date_map.keys())
        if max_block < blocknumber:
            max_block = None

    if blocknumber not in block_date_map:
        new_map = blocks.get_dates_for_blockchain(min_blocknumber=blocknumber,
                                                  max_blocknumber=max_block,
                                                  blockchain='ethereum')
        block_date_map.update(new_map)
    return block_date_map[blocknumber]


def get_ntx_erc20_retry(args):
    return maybe_error(maybe_retry, [get_ntx_erc20, [args]])


@timing
def get_ntx_erc20(args):
    name, addr, start_date = args
    start_block = blocks.get_min_blocknum_for_date('ethereum', start_date)

    logger.debug('Getting ntx data for %s at address %s starting from date %s' % (name, addr, start_date))
    transfers = xfers.get_transfers_for_token(addr, start_block, 'latest')

    if len(transfers) == 0:
        logger.warning('Zero transfers for address "%s" for token %s since %s' % (addr, name, start_date))
        ntxs = {}
    else:
        df = pd.DataFrame(transfers)
        df['date'] = df['blockNumber'].apply(lambda x: get_date_for_blocknum(int(x,16)))
        # ntxs = { 'datestring': ntx }
        ntxs = df.groupby(['date'])['address'].count().to_dict()

    # fill in zeros for empty dates
    for date in date_interval(start_date, datetime.date.today(), 1):
        datestring = date.strftime('%Y/%m/%d')
        if datestring not in ntxs:
            ntxs[datestring] = 0

    maybe(backfill_ntx_retry, [(name, ntxs)], num_processes=1)
    return name, ntxs

