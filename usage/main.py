from flask import Flask
from flask_api import status
from flask import Response
from apscheduler.schedulers.background import BackgroundScheduler
import datetime
from usage.sensors.eos import stream_blocks as stream_eos_blocks
from usage.sensors.ethereum import stream_blocks as stream_eth_blocks
from usage.usage_stats import UsageStats
from common.sensors import gsheet
from usage.config import backfill_all_ntxs
from usage.ntx import update_ntxs
import time
import logging
from common.config import format_logger, maybe_retry, gsheet_url, date_interval, earliest_crypto_date, date_interval_reverse


logger = logging.getLogger('main')
format_logger(logger)
# logging.getLogger('apscheduler').setLevel(logging.DEBUG)

app = Flask(__name__)

@app.route('/')
def root():
    return 'I am a teapot.', 420


@app.route('/health',  methods=['GET'])
def health():
    if not scheduler.running or scheduler.get_jobs().__len__() == 0:
        return Response("{}", status=status.HTTP_500_INTERNAL_SERVER_ERROR, mimetype='application/json')
    return Response("{}", status=status.HTTP_200_OK, mimetype='application/json')


def update_usage_stats():

    logger.info('Starting Update Usage Stats')

    coin_data = maybe_retry(gsheet.get_master_spreadsheet, gsheet_url)
    
    logger.info("Starting Updating usage stats... date: {} running NOW!".format(datetime.date.today()))

    logger.info('Coin Data Format:{} Sample Data:{}'.format(type(coin_data), coin_data[0]))

    logger.info('Updating ntxs. Backfill all = {}'.format(backfill_all_ntxs))
    
    update_ntxs(coin_data, parallel=True, backfill_all=backfill_all_ntxs)

    logger.info('Updating usage stats')
    
    Usage = UsageStats()
    
    Usage.update(datetime.date.today())

    logger.info("Finishing Updating usage stats... date: {} running NOW!".format(datetime.date.today()))


def backfill_usage_stats():

    try:

        logger.info("Starting Backfilling... date: {} running NOW!".format(datetime.date.today()))

        Usage = UsageStats()

        logger.info("Backfilling... Step 1 running NOW!")

        dates = date_interval_reverse(earliest_crypto_date, datetime.date.today(), 1)

        logger.info("Backfilling... date: {} Step 2 running NOW!".format(datetime.date.today()))
        
        logger.info("Backfilling... Step 3 dates: {}".format(dates))

        for date in dates:

            try:

                logger.info("Processing Backfilling... running {}".format(date))

                Usage.update(date)
            
            except Exception as error:

                logger.info("Found layer 2 ERROR: {}".format(str(error)))


    except Exception as error:

        logger.info("Found an ERROR: {}".format(str(error)))

def backfill_usage_stats_today():

    try:

        logger.info("Starting Backfilling 2.0 ... date: {} running NOW!".format(datetime.date.today()))

        Usage = UsageStats()

        logger.info("Backfilling 2.0... Step 1 running NOW!")
        
        logger.info("Backfilling 2.0... Step 2")

        try:

            logger.info("Processing Backfilling 2.0... running {}".format(datetime.date.today()))

            Usage.update(datetime.date.today())
        
        except Exception as error:

            logger.info("Backfilling 2.0 Found layer 2 ERROR: {}".format(str(error)))


    except Exception as error:

        logger.info("Backfilling 2.0 Found an ERROR: {}".format(str(error)))


now = datetime.datetime.utcnow()
jiggle = datetime.timedelta(seconds=15)
if __name__ == '__main__':
    scheduler = BackgroundScheduler()
    #scheduler.add_job(lambda: stream_eos_blocks(), trigger='interval', seconds=60, max_instances=1)
    #scheduler.add_job(lambda: stream_eth_blocks(), trigger='interval', seconds=60, max_instances=1)
    scheduler.add_job(
        func=update_usage_stats,
        trigger='interval',
        hours=6,
        max_instances=1,
        next_run_time=now + jiggle
    )
    scheduler.add_job(
        func=backfill_usage_stats,
        trigger='interval',
        hours=6,
        max_instances=1,
        next_run_time = now + jiggle,
    )

    scheduler.add_job(
        func=backfill_usage_stats,
        trigger='interval',
        hours=6,
        max_instances=1,
        next_run_time = now + jiggle,
    )

    jobs = scheduler.get_jobs()
    logger.debug('Starting scheduler with {} jobs'.format(len(jobs)))
    scheduler.start()
    time.sleep(1)
    try:
        app.run(host='0.0.0.0', port=5000)
    except Exception as e:
        scheduler.shutdown()
